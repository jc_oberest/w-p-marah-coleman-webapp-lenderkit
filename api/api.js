require('dotenv').config();

const server = require('lenderkit-webapp-api');

server.listen(process.env.PORT || '3000');
