/* NOTE! This file contains special comments fot autosetup.
  If you add any optional module to webapp application, please surround all the module lines with the next comments
  //module-{same-as-appropriate-backend-module-name}-lines-begin fot lines start
  //module-{same-as-appropriate-backend-module-name}-lines-end fot lines finish
  Note, that modulec-module comments has NO space after //
*/









//module-equity-lines-begin
import Equity from '@equity/Installer';
//module-equity-lines-end

export const enabledModules = [










//module-equity-lines-begin
  'equity',
//module-equity-lines-end
];

export default [








//module-equity-lines-begin
  Equity,
//module-equity-lines-end
];
