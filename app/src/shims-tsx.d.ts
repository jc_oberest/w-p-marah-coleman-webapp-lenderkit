import Vue, {VNode} from 'vue';
import Form from '@core/forms/components/Form.vue';
import {CacheInstance} from '@core/cache/types';

declare global {
  namespace JSX {
    // tslint:disable no-empty-interface
    interface Element extends VNode {
    }

    // tslint:disable no-empty-interface
    interface ElementClass extends Vue {
    }

    interface IntrinsicElements {
      [elem: string]: any;
    }
  }
}

interface FormsProperty {
  [key: string]: Form;
}

declare module 'vue/types/vue' {

  interface Vue {
    $forms: FormsProperty;
    $cache: CacheInstance;
    appLoading: boolean;
    appOffline: boolean;
    isFormInput: boolean;
    // @ts-ignore
    errors: any;
  }
}
