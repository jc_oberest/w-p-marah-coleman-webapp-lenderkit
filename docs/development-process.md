# Development process

## Configure IDE (PHPStorm or WebStorm)

* Configure _Settings > Version Control_ sources for all git submodules
* Turn off "Safe write" mode in _Settings > Appearance & Behavior > System Settings"
* On your host system (Linux or Mac OS) turn off file limits with `sudo launchctl limit maxfiles unlimited unlimited`
* Mark folders as "Excluded" (if they exists): `app/dist`, `app/lenderkit`, `api/lenderkit`

## Basics
The LenderKit WebApp is used as a dependency and so it is stored inside `node_modules`. 
You can't edit original LenderKit WebApp!
To update something, which already exists inside LenderKit WebApp you need to override corresponding file 
inside your project.

## Overriding
To override the LenderKit's file you need to do the following:
* open `app\vue.config.js`
* inside `configureWebpack` callback search for object key `module.rules[0].use.options.replace`
* update `replace` object. The key is the path to LenderKit origin file, value - is the path to your new file  
_(path to your file can be different from overrided file)_
* re-build the project again. (if you're in "watch" mode - you need to restart watch for these changes to take effect)

_**Note:** Your local files SHOULD NOT contain comment with `@package lenderkit-webapp`_

You can override any file from the LenderKit: routes, models, classes to work with API and so on.

If you need to make changes in the template only without changes in the functionality, 
you can import this component from the LenderKit and extend your class from it, 
so the component will use your template and functionality from the LenderKit-component.
The same if you need to make changes in the functionality without changes in the template.
